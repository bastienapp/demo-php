<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Démo live coding</h1>
  <?php
    // j'ai appelé ma base de données et j'ai récupéré les informations de l'utilisateur connecté
    $connected_user_id = $_GET["identifiant"];
    $user_list = [
      0 => [
        "email" => "bastien@simplon.co",
        "first_name" => "Bastien",
        "last_name" => "Krettly"
      ],
      1 => [
        "email" => "nicolas@simplon.co",
        "first_name" => "Nicolas",
        "last_name" => "Piquet"
      ]
    ];
  ?>
  <main>
    <section>
      <label>
        Email:
        <input type="email" value="<?php echo $user_list[$connected_user_id]["email"]; ?>" />
      </label>
      <br /><br />
      <label>
        Prénom:
        <input type="text" value="<?php echo $user_list[$connected_user_id]["first_name"]; ?>" />
      </label>
      <br /><br />
      <label>
        Nom:
        <input type="text" value="<?php echo $user_list[$connected_user_id]["last_name"]; ?>" />
      </label>
      <br />
    </section>
  </main>
</body>
</html>